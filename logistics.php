<?php
/*
Plugin Name: iFleet Logistics for Woocommerce
Plugin URI: http://icommerce.asia
Description: iCommerce Logistics.
Author: iCommerce Asia
Version: 1.0
Author URI: http://icommerce.asia
*/


if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
}

define('IFLEET_PATH', plugin_dir_path(__FILE__));

include(IFLEET_PATH.'/class-opskii.php');
include(IFLEET_PATH.'/vendor/autoload.php');
add_action( 'plugins_loaded', 'opskii_initialize_plugin' );

function opskii_initialize_plugin() {

    global $opskii, $ifleet_api_key, $ifleet_api_url;
    $opskii = new Opskii();
    $ifleet_api_key = get_option('ifleet_api_key');
    $ifleet_api_url = 'http://opskii.com/api/v1/book-shipment/one-to-many';
    $ifleet_test_mode = get_option('ifleet_test_mode');
    if($ifleet_test_mode == 'yes'){
        $ifleet_api_url = 'http://dev.opskii.com/api/v1/book-shipment/one-to-many';
        $ifleet_api_key = '31025d3d00da98cc3dbc2df9fceb1ba4';
    }


}


register_activation_hook( __FILE__, 'logistics_plugin_init' );

function logistics_plugin_init(){

    flush_rewrite_rules();

}

add_action( 'woocommerce_init', 'ifleet_logistics' );

function ifleet_logistics(){

    global $current_user;
    require_once 'address-option.php';
}


add_action( 'wp_ajax_inform_shipping', 'inform_shipping' );

function inform_shipping(){

    global $opskii,$ifleet_api_key, $ifleet_api_url;
    if($ifleet_api_key == ''){
        $msg = array('status'=>2);
        //Please contact iCommerce staff to get opskii api key.';
    }
    else{
        $jobId = $opskii->process_shipping($_POST['id']);
        if($jobId){
            $order = new WC_Order( $_POST["id"] );
            $msg = array('status'=>1);
            //Add customer note to order, this triggers an email to the customer.
            $tracking_id = get_post_meta($_POST["id"],'ifleet_tracking_id',true);
            $body   = 'We have initiated shipping for your order. ';
            $body   .= '<p>You can track it here <a href="http://ifleet.asia/track/'.$tracking_id.'">http://ifleet.asia/track/'.$tracking_id .'</p>';
            $order->add_order_note($body,1);
        }
        else{
            $msg = $jobId ;
        }
    }
    wp_send_json( $msg );
    die();

}



add_filter( 'manage_edit-shop_order_columns', 'add_new_coln' );

function add_new_coln($columns){

    $new_columns = (is_array($columns)) ? $columns : array();
    unset( $new_columns['order_actions'] );
    $new_columns['REZKII_COLUMN_ID_1'] = 'iFleet';
    $new_columns['order_actions'] = $columns['order_actions'];
    return $new_columns;

}

add_action( 'manage_shop_order_posts_custom_column', 'order_column_content', 2 );

function order_column_content($column){

    global $post,$opskii, $ifleet_api_key;
     $screen = get_current_screen();
    if($screen->id == 'edit-shop_order'){
        wp_register_style( 'shipping', plugin_dir_url( __FILE__ ) . 'css/shipping.css', array(), '1.1', 'all' );
        wp_enqueue_style( 'shipping' );
        wp_register_script( 'ifleet', plugin_dir_url( __FILE__ ) . 'js/ifleet.js', array('jquery'),'1' );

        $shipping_det = array(
			'plugin_url' => plugin_dir_url( __FILE__ ),
			'site_url' => site_url(),
            'nonce' => wp_create_nonce('ifleet-ship'),
            'ajaxurl' => admin_url( 'admin-ajax.php' )
		);
		wp_localize_script( 'ifleet', 'plugin_det', $shipping_det );
        wp_enqueue_script( 'ifleet' );

    }
    if ( $column == 'REZKII_COLUMN_ID_1' ) {

        $is_processed = get_post_meta($post->ID,'ifleet_tracking_id',true);
        if($is_processed){
            echo '<div class="btn btn-success" >Informed</div>';
        }
        else{
            if($ifleet_api_key != ''){
                echo '<a class="btn btn-primary process" data-orderId="'.$post->ID.'">Process</a>';
            }

        }
    }
}