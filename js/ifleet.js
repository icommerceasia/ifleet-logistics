(function($){
    var $hb = $('html, body'),
        $b = $('body'),
        $d = $(document);
	
    $d.ready(function () {
        
        $(".process").click( function() {
            console.log('check');
            console.log($(this).data("orderid"));
            var $orderId = $(this).data("orderid");
            console.log(plugin_det.site_url);
            var $theBtn = $(this);
            
            $.ajax({
                    type: 'post',
                    url: plugin_det.ajaxurl,
                    data: {
                        action: 'inform_shipping',
                        id: $orderId, nonce: plugin_det.nonce },
                    beforeSend: function() {
                       $theBtn.after('<p id="processing"><i class="fa fa-spinner fa-pulse" aria-hidden="true"></i> Processing</p>');
                       $theBtn.remove();
                    },
                    complete: function(data) {
                    },

                    })
                    .done(function( data ) {
                        var sts = data.status;
                        if(sts == 1){
                            $('#processing').after('<div class="btn btn-success" >Informed</div>');
                            $('#processing').remove();
                        }
                        else{
                            $('#processing').replaceWith('There seems to be some issues with logistics processing. Please report this to iCommerce');
                        }
                    });
        });
        
    });

}(jQuery));



