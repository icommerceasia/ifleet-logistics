<?php

class Opskii {

    public function getTrackingId($tasks,$invoice_no){
        $tracking_id = 0;
        foreach($tasks as $task){
            if($invoice_no == $task->invoice_number){
                $tracking_id = $task->tracking_id;
                break;
            }
        }
        return $tracking_id;
    }


    public function process_shipping($orderId){

        global $ifleet_api_url;
        $jobId = 0;
        $client = new GuzzleHttp\Client();
        $data = $this->prepareDetails($orderId);
        $url = $ifleet_api_url;
        $request = $client->request('post', $url, ['json' => $data]);
        $jobDet = $request->getBody();
        $jobResponse = json_decode($jobDet);
        if(isset($jobResponse->job)){
            $jobId = $jobResponse->job->id;
            $tasks = $jobResponse->job->tasks;
            $trackingId = $this->getTrackingId($tasks,$orderId);
            update_post_meta($orderId, 'ifleet_tracking_id', $trackingId);
            update_post_meta($orderId, 'ifleet_job_id', $jobId);
        }

        return $jobId;

    }


    function prepareDetails($orderId){

        global $ifleet_api_key;
        $order = new WC_Order( $orderId );
        $data['pickup_details'] = $this->preparePickupDetails($order);
        $data['delivery_details'] = $this->prepareDeliveryDetails($order);
        $data['api_key'] = $ifleet_api_key;
        return $data;
    }


    function preparePickupDetails($order){

        $pickup_attributes['tracking_id'] = '';
        $pickup_attributes['price'] = $order->order_total;

        if(get_option('ifleet_ware_house') == 'yes'){
            $pickup_attributes['remarks'] = "iCommerce Merchant- Collect from icommerce warehouse and deliver to buyer";
            $pickup_address_attributes['name'] = get_option('shop_contact'). ' - iCommerce';
            $pickup_address_attributes['line_1'] = '27 Greenwich Drive, Annexe Building, Level 2';
            $pickup_address_attributes['zip'] = '533912';
            $pickup_address_attributes['country'] = 'SG';
            $pickup_address_attributes['email'] = get_option('shop_email');
            $pickup_address_attributes['contact_person'] = get_option('shop_contact');
            $pickup_address_attributes['contact_number'] = get_option('shop_phno');
            $pickup_attributes['address_attributes'] = $pickup_address_attributes;
            $pickup_attributes['tag_list'] = ["pick and pack","ifleet"];
        }
        else{
            $address_line2 = get_option('shop_address_line2');
            $address = isset($address_line2)? get_option('shop_address_line1'). " , ".$address_line2 : get_option('shop_address_line1');
            $pickup_details['remarks'] = "iCommerce Merchant- Collect from merchant and deliver to iCommerce warehouse";
            $pickup_address_attributes['name'] = get_option('shop_contact');
            $pickup_address_attributes['line_1'] = $address;
            $pickup_address_attributes['zip'] = get_option('shop_zip');
            $pickup_address_attributes['country'] = 'SG';
            $pickup_address_attributes['email'] = get_option('shop_email');
            $pickup_address_attributes['contact_person'] = get_option('shop_contact');;
            $pickup_address_attributes['contact_number'] = get_option('shop_phno');
            $pickup_attributes['address_attributes'] = $pickup_address_attributes;
            $pickup_attributes['tag_list'] = ["pickup","ifleet"];

        }
        return $pickup_attributes;

    }


    function prepareDeliveryDetails($order){

        $address_line1 = $order->shipping_address_1;
        $address_line2 = $order->shipping_address_2;
        $city = $order->shipping_city;
        $state = $order->shipping_state;

        $address = empty($address_line2) ? $address_line1 : $address_line1. ',' .$address_line2 ;
        $address = empty($city) ? $address : $address.', '.$city;
        $address = empty($state) ? $address : $address.', '.$state;

        $delivery['price'] = $order->order_total;
        $delivery['invoice_no'] = $order->id;
       // $delivery['tracking_id'] = 'IDS'.date('Ymd').$order->id;
        $delivery['remarks'] = $order->customer_message;
        $delivery_address_attributes['name'] = $order->get_formatted_shipping_full_name();
        $delivery_address_attributes['line_1'] = $address;
        $delivery_address_attributes['zip'] = $order->shipping_postcode;
        $delivery_address_attributes['country'] = $order->shipping_country;
        $delivery_address_attributes['email'] = $order->shipping_email;
        $delivery_address_attributes['contact_person'] = $order->get_formatted_shipping_full_name();
        $delivery_address_attributes['contact_number'] = $order->billing_phone;
        $delivery['address_attributes'] = $delivery_address_attributes;
        $delivery['measurement_attributes'] = $this->getMeasurementAttr($order);
        $delivery['tag_list'] = ["delivery","ifleet"];
        $delivery_attributes = array();
        array_push($delivery_attributes,$delivery);
        return $delivery_attributes;

    }

    function getMeasurementAttr($order){

        $measurements_attributes = array();
        $items = $order->get_items();
        foreach( $items as $product ) {
            $item_metas = get_post_meta( $product['product_id'] );
            $weight = $item_metas['_weight']['0'];
            $length = $item_metas['_length']['0'];
            $width = $item_metas['_width']['0'];
            $height = $item_metas['_height']['0'];
            list($unit,$weight) = $this->getUnitWeight($length,$width,$height,$weight);
            $prodct_name[] = $product['name'];
            $attribute['quantity'] = $weight;
            $attribute['quantity_unit'] = $unit;
            $attribute['description'] = $product['name']. 'X'.$product['qty'];
            array_push($measurements_attributes,$attribute);
        }
        return $measurements_attributes;

    }

    function getUnitWeight($length,$width,$height,$weight){
        $weight_unit = get_option('woocommerce_weight_unit');
        $dimension_unit = get_option('woocommerce_dimension_unit');
        //convert unit to kg

        if($weight_unit == 'g'){
            $wt_multiply = 0.001;
        }
        elseif($weight_unit == 'kg'){
            $wt_multiply = 1;
        }
        elseif($weight_unit == 'lbs'){
            $wt_multiply = 0.45;
        }
        elseif($weight_unit == 'oz'){
            $wt_multiply = 0.028;
        }

        //convert dimension to cm

        if($dimension_unit == 'm'){
            $dm_multiply = 100;
        }
        elseif($dimension_unit == 'cm'){
            $dm_multiply = 1;
        }
        elseif($dimension_unit == 'mm'){
            $dm_multiply = 0.1;
        }
        elseif($dimension_unit == 'in'){
            $dm_multiply = 2.54;
        }
        elseif($dimension_unit == 'yd'){
            $dm_multiply = 91.44;
        }

        $weight = $weight * $wt_multiply;
        $dimension = $length  * $width * $height * $dm_multiply / 1000;
        if($weight > $dimension){
            $unitWeight = array('kg',$weight);
        }
        else{
            $unitWeight = array('cm',$dimension);
        }
        return $unitWeight;
    }

}