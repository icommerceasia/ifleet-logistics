## Purpose
This plugin is an add on plugin for Woo commerce based sites to automate logistics via iFleet

#### Installation instructions

1. Woocommerce plugin is a prerequisite

2. Install the plugin like any other wordpress plugins

3. Please fill the fields under woocommerce
	- Settings - Shipping (For woocommerce 2.5.5)
	- Settings - Shipping - Shipping Options (For woocommerce 2.6)

4. Contact http://ifleet.asia to get the ifleet api key