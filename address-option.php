<?php
add_filter( 'woocommerce_shipping_settings', 'add_store_address' );



function add_store_address( $settings ) {

  $updated_settings = array();

  foreach ( $settings as $section ) {

    // at the bottom of the General Options section

    if ( isset( $section['id'] ) && 'shipping_options' == $section['id'] &&

       isset( $section['type'] ) && 'sectionend' == $section['type']  ) {



      $updated_settings[] = array(

        'name'     => __( 'Warehouse Address Line1', 'wc_address_line1' ),

        'desc_tip' => __( 'Warehouse Pickup  Address line1' ),

        'id'       => 'shop_address_line1',

        'type'     => 'text',

        'css'      => 'min-width:300px;',

      );
	  $updated_settings[] = array(

        'name'     => __( 'Warehouse Address Line2', 'wc_address_line2' ),

        'desc_tip' => __( 'Warehouse Address line2 of your store' ),

        'id'       => 'shop_address_line2',

        'type'     => 'text',

        'css'      => 'min-width:300px;',

      );
	  $updated_settings[] = array(

        'name'     => __( 'Warehouse Zip code', 'shop_zip' ),

        'desc_tip' => __( 'Warehouse Zip Code' ),

        'id'       => 'shop_zip',

        'type'     => 'text',

        'css'      => 'min-width:300px;',

      );
	  $updated_settings[] = array(

        'name'     => __( 'Country', 'shop_country' ),

        'desc_tip' => __( 'Country' ),

        'id'       => 'shop_country',

        'type'     => 'text',

        'css'      => 'min-width:300px;',

      );
     $updated_settings[] = array(

        'name'     => __( 'Email address of person in charge (warehouse)', 'shop_email' ),

        'desc_tip' => __( 'Email address of person in charge (warehouse)' ),

        'id'       => 'shop_email',

        'type'     => 'text',

        'css'      => 'min-width:300px;',

      );
     $updated_settings[] = array(

        'name'     => __( 'Name of person in charge (warehouse)', 'shop_contact' ),

        'desc_tip' => __( 'of person in charge (warehouse)' ),

        'id'       => 'shop_contact',

        'type'     => 'text',

        'css'      => 'min-width:300px;',

      );
    $updated_settings[] = array(

        'name'     => __( 'Phone number of person in charge (warehouse)', 'shop_phno' ),

        'desc_tip' => __( 'Phone number of person in charge (warehouse)' ),

        'id'       => 'shop_phno',

        'type'     => 'text',

        'css'      => 'min-width:300px;',

      );
	  $updated_settings[] = array(

        'name'     => __( 'I want iCommerce to handle intenational Delivery', 'international_delivery' ),

     //   'desc_tip' => __( 'I want iCommerce to handle intenational Delivery' ),

        'id'       => 'international_delivery',

        'type'     => 'checkbox',

        'css'      => 'min-width:300px;',

      );
	  $updated_settings[] = array(

        'name'     => __( 'I want to use iCommerce warehouse', 'ifleet_ware_house' ),

    //    'desc_tip' => __( 'I want to use iCommerce warehouse' ),

        'id'       => 'ifleet_ware_house',

        'type'     => 'checkbox',

        'css'      => 'min-width:300px;',

      );

    $updated_settings[] = array(

        'name'     => __( 'iFleet Api Key', 'ifleet_api_key' ),

        'desc_tip' => __( 'Please contact iCommerce to get the api key' ),

        'id'       => 'ifleet_api_key',

        'type'     => 'text',

        'css'      => 'min-width:300px;',

    );

    $updated_settings[] = array(

        'name'     => __( 'Opskii Token', 'opskii_token' ),

        'desc_tip' => __( 'Please contact iCommerce to get the Opskii Token' ),

        'id'       => 'opskii_token',

        'type'     => 'text',

        'css'      => 'min-width:500px;',

    );

    $updated_settings[] = array(

        'name'     => __( 'iFleet Tracking Prefix', 'ifleet_tracking_prefix' ),

        'desc_tip' => __( 'This prefix will be prepended to order id.' ),

        'id'       => 'ifleet_tracking_prefix',

        'type'     => 'text',

        'css'      => 'min-width:500px;',

    );

    $updated_settings[] = array(

        'name'     => __( 'iFleet Logistics test mode', 'ifleet_test_mode' ),

        'desc_tip' => __( 'Please check this box if you want to test some orders.' ),

        'id'       => 'ifleet_test_mode',

        'type'     => 'checkbox',

        'css'      => 'min-width:300px;',

    );

    }



    $updated_settings[] = $section;

  }

  return $updated_settings;

}
